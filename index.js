const async = require('async');
const client = require('./client');
const parser = require('./parser');

const rawLookup = client.lookup;

const lookup = (domain, callback) => {
  client.lookup(domain, function(err, data) {
    return callback(err, parser.parse(data));
  });
}

const multiLookup = (domains, callback) => {
  async.mapLimit(domains, 100, lookup, function(err, data) {
    if (data) {
      const result = {};
      for (let i = 0; i < domains.length; i++) {
        result[domains[i]] = data[i];
      }
      return callback(err, result);
    } else {
      return callback(err);
    }
  });
}

module.exports = {
  rawLookup,
  lookup,
  multiLookup
}
